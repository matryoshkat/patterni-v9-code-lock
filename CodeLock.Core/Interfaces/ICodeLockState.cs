
namespace CodeLock.Core.Interfaces
{
    public interface ICodeLockState
    {
        void SetContext(CodeLock codeLock);
        bool CheckCode(string digitsCode);
        void SendSignal();
        /// <summary>
        /// Modifier is hodling signal button
        /// </summary>
        void SendModifier();
        void ControlHandler();
    }
}