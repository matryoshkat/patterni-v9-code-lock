namespace CodeLock.Core.Interfaces
{
    public interface IPasswordManager
    {
        string GetSecurityCode();
        string GetControlCode();
        void SetSecurityCode(string code);
        void SetControlCode(string code);
    }
}