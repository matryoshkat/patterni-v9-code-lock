namespace CodeLock.Core.Interfaces
{
    public interface ICodeLock
    {
        void TransitionTo(ICodeLockState state);
        void TakeDigitCode(string code);
        void CallButtonHandler();
        void CallButonHoldHandler();
        void ControlButtonHandler();
        void OpenLock();
        void SendSignal();
        void ResetAutoLockTimer();
        void SetAutoLockTimer();
    }
}