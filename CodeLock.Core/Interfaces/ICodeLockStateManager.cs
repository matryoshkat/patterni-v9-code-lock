namespace CodeLock.Core.Interfaces
{
    public interface ICodeLockStateManager
    {
        ICodeLockState GetState<T>(CodeLock context) where T : class, ICodeLockState;
    }
}