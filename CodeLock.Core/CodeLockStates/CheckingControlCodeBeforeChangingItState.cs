using CodeLock.Core.CodeLockStates.Base;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core.CodeLockStates
{
    public class CheckingControlCodeBeforeChangingItState : BaseState, ICodeLockState
    {
        private readonly ICodeLockStateManager _codeLockStateManager;
        private readonly IPasswordManager _passwordManager;

        public CheckingControlCodeBeforeChangingItState(
            ICodeLockStateManager codeLockStateManager,
            IPasswordManager passwordManager)
        {
            _codeLockStateManager = codeLockStateManager;
            _passwordManager = passwordManager;
        }
        
        public new bool CheckCode(string digitsCode)
        {
            if (digitsCode != _passwordManager.GetControlCode())
            {
                _context.TransitionTo(
                    _codeLockStateManager.GetState<OpenedState>(_context));
                _context.SetAutoLockTimer();
                return false;
            }

            _context.TransitionTo(
                _codeLockStateManager.GetState<ChangingControlCodeState>(_context));
            return true;
        }
    }
}