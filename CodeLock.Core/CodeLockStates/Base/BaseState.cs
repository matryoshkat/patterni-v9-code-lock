using System.Collections.Generic;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core.CodeLockStates.Base
{
    public abstract class BaseState : ICodeLockState
    {
        private protected CodeLock _context;
        public void SetContext(CodeLock codeLock)
        {
            _context = codeLock;
        }

        public bool CheckCode(string digitsCode)
        {
            return false;
        }

        public void SendSignal()
        {
            _context.SendSignal();
        }

        public void SendModifier()
        {
            _context.SendSignal();
        }

        public void ControlHandler()
        {
            
        }
    }
}