using CodeLock.Core.CodeLockStates.Base;
using CodeLock.Core.Interfaces;

namespace CodeLock.Core.CodeLockStates
{
    public class OpenedState : BaseState, ICodeLockState
    {
        
        private readonly ICodeLockStateManager _codeLockStateManager;

        public OpenedState(ICodeLockStateManager codeLockStateManager)
        {
            _codeLockStateManager = codeLockStateManager;
        }
        
        
        public new void ControlHandler()
        {
            _context.ResetAutoLockTimer();
            _context.TransitionTo(
                _codeLockStateManager.GetState<ControlState>(_context));
            
        }
        
    }
}