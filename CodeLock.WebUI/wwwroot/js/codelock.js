let displayClearTimeout;
let signalBtnTimeout;
let isSignalModifierSent = false;

function addHandlersToDigitButtons(button) {
    const displayEl = document.getElementById("display");
    const event = new Event("change");
    
    button.addEventListener("click", () => {
        clearTimeout(displayClearTimeout);
        displayClearTimeout = setTimeout(() => clearDisplay(displayEl), 1000);
        const buttonNumber = button.innerHTML;
        displayEl.value += buttonNumber;
        displayEl.dispatchEvent(event);
    });
}

function onDisplayChange(displayEl) {
    const text = displayEl.value;
    if(text.length === 4){
        sendEnteredValue(text);
        return;
    }
    if (text.length > 4) {
        displayEl.value = text[text.length - 1];
    }
}

function clearDisplay(displayEl){
    displayEl.value = "";
}

function sendEnteredValue(value){
    console.log(value);
    connection.invoke("TakeCode", value);
}

function onSignalBtnOnDown(btn){
    signalBtnTimeout = setTimeout(() => {
        connection.invoke('CallButtonHold');
        isSignalModifierSent = true;
        console.log("sent CallButtonHold");
    }, 1000);
}

function onSignalBtnOnUp(btn){
    clearTimeout(signalBtnTimeout);
    if(!isSignalModifierSent){
        connection.invoke('CallButton');
        console.log("sent CallButton");
    }
    isSignalModifierSent = false;
}

(function () {
    const displayElement = document.getElementById("display");
    displayElement.addEventListener("change", () => onDisplayChange(displayElement));
    const buttons = document.getElementsByClassName("digit-button");
    const signalBtn = document.getElementById("signal-btn");
    // signalBtn.addEventListener("click", (e) => onSignalBtnOnUp(e));
    signalBtn.addEventListener('mousedown', (e) => onSignalBtnOnDown(e));
    signalBtn.addEventListener('mouseup', (e) => onSignalBtnOnUp(e));
    [...buttons].forEach(addHandlersToDigitButtons);
})();

