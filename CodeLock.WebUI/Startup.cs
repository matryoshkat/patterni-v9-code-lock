using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CodeLock.Core;
using CodeLock.Core.CodeLockStates;
using CodeLock.Core.Interfaces;
using CodeLock.WebUI.Application;
using CodeLock.WebUI.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CodeLock.WebUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ICodeLockStatesFactory, CodeLockStatesFactory>();
            services.AddSingleton<ICodeLockStateManager, CodeLockStateManager>();
            services.AddSingleton<ILockStatusNotifier, LockStatusNotifier>();
            services.AddSingleton<IPasswordManager, DefaultPasswordManager>();
            services.AddSingleton<ISignalNotifier, SignalNotifier>();
            services.AddSingleton<ICodeLock, Core.CodeLock>();
            services.AddRazorPages()
                .AddRazorRuntimeCompilation();
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapHub<CodeLockHub>("/codeLockHub");
            });
        }
    }
}