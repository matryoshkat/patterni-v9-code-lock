using System.Threading.Tasks;
using CodeLock.Core.Interfaces;
using Microsoft.AspNetCore.SignalR;

namespace CodeLock.WebUI.Hubs
{
    public class CodeLockHub : Hub
    {
        private readonly ICodeLock _codeLock;

        public CodeLockHub(ICodeLock codeLock)
        {
            _codeLock = codeLock;
        }

        public Task TakeCode(string code)
        {
            return Task.Run(() => _codeLock.TakeDigitCode(code));
        }

        public Task CallButton()
        {
            return Task.Run(() => _codeLock.CallButtonHandler());
        }

        public Task CallButtonHold()
        {
            return Task.Run(() => _codeLock.CallButonHoldHandler());
        }

        public Task ControlButton()
        {
            return Task.Run(() => _codeLock.ControlButtonHandler());
        }
    }
}