using System.Threading.Tasks;
using CodeLock.Core.Interfaces;
using CodeLock.WebUI.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace CodeLock.WebUI.Application
{
    public class SignalNotifier : ISignalNotifier
    {
        private readonly IHubContext<CodeLockHub> _hubContext;

        public SignalNotifier(IHubContext<CodeLockHub> hubContext)
        {
            _hubContext = hubContext;
        }
        public void Notify()
        {
            Task.Run(() => _hubContext.Clients.All.SendAsync("SignalCalled"));
        }
    }
}