using System.Threading.Tasks;
using CodeLock.Core.Interfaces;
using CodeLock.WebUI.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace CodeLock.WebUI.Application
{
    public class LockStatusNotifier : ILockStatusNotifier
    {
        private readonly IHubContext<CodeLockHub> _hub;


        public LockStatusNotifier(IHubContext<CodeLockHub> hub)
        {
            _hub = hub;
        }
        
        public void LockOpened()
        {
            Task.Run(() => _hub.Clients.All.SendAsync("LockOpened"));
        }

        public void LockClosed()
        {
            Task.Run(() => _hub.Clients.All.SendAsync("LockClosed"));
        }
    }
}