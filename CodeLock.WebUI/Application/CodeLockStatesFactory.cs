using System;
using CodeLock.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CodeLock.WebUI.Application
{
    public class CodeLockStatesFactory : ICodeLockStatesFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public CodeLockStatesFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public T CreateLockState<T>() where T : class, ICodeLockState
        {
            return ActivatorUtilities.CreateInstance<T>(_serviceProvider);

        }
    }
}