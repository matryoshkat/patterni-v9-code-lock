using System;
using System.Collections.Generic;
using CodeLock.Core.CodeLockStates;
using CodeLock.Core.Interfaces;
using Moq;
using NUnit.Framework;

namespace CodeLock.Core.Tests
{
    public class Tests
    {
        [Test]
        public void ReturnState()
        {
            var codeLock = GetTestCodeLock();
            var simpleFactory = new SimpleCodeLockFactory();
            var manager = new CodeLockStateManager(simpleFactory);
            var state = manager.GetState<TestState>(codeLock);
            Assert.NotNull(state);
        }


        [Test]
        public void ReturnSameState()
        {
            var codeLock = GetTestCodeLock();
            var simpleFactory = new SimpleCodeLockFactory();
            var manager = new CodeLockStateManager(simpleFactory);
            var state = manager.GetState<TestState>(codeLock);
            var shouldBeTheSameState = manager.GetState<TestState>(codeLock);
            Assert.AreEqual(state, shouldBeTheSameState);
        }

        private CodeLock GetTestCodeLock()
        {
            var lockStatusNotifier = Mock.Of<ILockStatusNotifier>();
            var signalStatusNotifier = Mock.Of<ISignalNotifier>();
            var stateManager = new Mock<ICodeLockStateManager>();
            stateManager.Setup(m =>
                m.GetState<ClosedState>(It.IsAny<CodeLock>())).Returns(new TestState());

            return new CodeLock(lockStatusNotifier, stateManager.Object, signalStatusNotifier);
        }

        class SimpleCodeLockFactory : ICodeLockStatesFactory
        {
            public T CreateLockState<T>() where T : class, ICodeLockState
            {
                var testState = new TestState();
                return testState as T;
            }
        }

        class TestState : ICodeLockState
        {
            public void SetContext(CodeLock codeLock)
            {
                throw new NotImplementedException();
            }

            public bool CheckCode(string digitsCode)
            {
                throw new System.NotImplementedException();
            }

            public void SendSignal()
            {
                throw new System.NotImplementedException();
            }

            public void SendModifier()
            {
                throw new System.NotImplementedException();
            }

            public void ControlHandler()
            {
                throw new System.NotImplementedException();
            }
        }
    }
}